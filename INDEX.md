# SHSUFDRV

SHSUFDRV is a driver for floppy and hard disk images. SHSURDRV copies the image to RAM and/or creates RAM drives.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## SHSUFDRV.LSM

<table>
<tr><td>title</td><td>SHSUFDRV</td></tr>
<tr><td>version</td><td>1.02a</td></tr>
<tr><td>entered&nbsp;date</td><td>2005-12-22</td></tr>
<tr><td>description</td><td>Driver for floppy and hard disk images</td></tr>
<tr><td>keywords</td><td>driver, floppy, images, hard disk, ram drive</td></tr>
<tr><td>author</td><td>Jason Hood</td></tr>
<tr><td>maintained&nbsp;by</td><td>Jason Hood &lt;jadoxa -AT- yahoo.com.au&gt; (looking for a new maintainer)</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>Freeware / source code available</td></tr>
<tr><td>summary</td><td>SHSUFDRV is a driver for floppy and hard disk images. SHSURDRV copies the image to RAM and/or creates RAM drives.</td></tr>
</table>
